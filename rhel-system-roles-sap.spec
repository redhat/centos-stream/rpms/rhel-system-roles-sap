# disable collection_artifact by default
%bcond_with collection_artifact

# disable collection by default since version 3.6.0 (1.3.5)
%bcond_with collection

%global collection_name_sap_install sap_install
%global collection_name_sap_infrastructure sap_infrastructure

%if 0%{?rhel}
%define package_name rhel-system-roles-sap
%global collection_namespace redhat
%else
%define package_name linux-system-roles-sap
%global collection_namespace community
%endif

# collection_version has to be increased by every rebuild
# otherwise, it cannot be uploaded to Automation Hub and Galaxy
# due to version conflict
%define collection_sap_install_version 1.5.2
%define collection_sap_infrastructure_version 1.1.1

%global collection_dir_sap_install collections/ansible_collections/%{collection_namespace}/%{collection_name_sap_install}
%global collection_dir_sap_infrastructure collections/ansible_collections/%{collection_namespace}/%{collection_name_sap_infrastructure}
%global mainid e4e63cd408561b39e11d6285ab81f32a999719db
%global commit_id_sap_install %{collection_sap_install_version}
%global commit_id_sap_infrastructure %{collection_sap_infrastructure_version}
%global rolename1 sap_general_preconfigure
%global rolename2 sap_ha_install_hana_hsr
%global rolename3 sap_hana_install
%global rolename4 sap_hana_preconfigure
%global rolename5 sap_ha_pacemaker_cluster
%global rolename6 sap_hypervisor_node_preconfigure
%global rolename7 sap_netweaver_preconfigure
%global rolename8 sap_swpm
%global rolename9 sap_vm_preconfigure
%global rolename10 sap_maintain_etc_hosts
%global src_owner linux-system-roles
%global github_repo_sap_install redhat.sap_install
%global github_repo_sap_infrastructure redhat.sap_infrastructure
%global rolename_sap_install %{rolename1} %{rolename2} %{rolename3} %{rolename4} %{rolename5} %{rolename7} %{rolename8} %{rolename10} 
%global rolename_sap_infrastructure %{rolename6} %{rolename9}
%global rolenames %{rolename_rhel}

Name: %{package_name}
Summary: System Roles to configure RHEL for running SAP NetWeaver- or SAP HANA-based products
Version: 3.8.5
Release: 1%{?dist}
License: GPL-3.0-or-later
Url: https://github.com/redhat-sap/community.sap_install
Source0: https://github.com/linux-system-roles/auto-maintenance/archive/%{mainid}/auto-maintenance-%{mainid}.tar.gz
Source1: https://github.com/redhat-sap/redhat.sap_install/archive/%{commit_id_sap_install}/%{github_repo_sap_install}-%{commit_id_sap_install}.tar.gz
Source2: https://github.com/redhat-sap/redhat.sap_infrastructure/archive/%{commit_id_sap_infrastructure}/%{github_repo_sap_infrastructure}-%{commit_id_sap_infrastructure}.tar.gz

# add tests files
Patch1: redhat.sap_install-1.3.4-add_tests.patch

# add tools files
Patch2: redhat.sap_install-1.3.4-add_tools.patch

# Fix check mode for sysctl
Patch3: redhat.sap_install-1.5.2-check_mode.patch

BuildArch: noarch

Requires: rhel-system-roles

BuildRequires: tar

# Requirements for galaxy_transform.py
BuildRequires: python3-ruamel-yaml
Requires: python3-jmespath
Requires: python3-netaddr

# NOTE: ansible-core is in rhel-8.6 and newer, but not installable
# in buildroot as it depended on modular Python.
# It has been installable at buildtime in 8.8 and newer.
%if %{with collection}
%if 0%{?rhel} >= 8
BuildRequires: ansible-core >= 2.11.0
Requires: (ansible-core >= 2.11.0 or ansible >= 2.9.0)
%endif
%if %{undefined __ansible_provides}
Provides: ansible-collection(%{collection_namespace}.%{collection_name}) = %{version}
%endif
# be compatible with the usual Fedora Provides:
Provides: ansible-collection-%{collection_namespace}-%{collection_name} = %{version}-%{release}
%endif

# ansible-galaxy is available by ansible-core on RHEL 8.6 and newer at buildtime.
%define ansible_collection_build() ansible-galaxy collection build
%define ansible_collection_install() ansible-galaxy collection install -n -p %{buildroot}%{_datadir}/ansible/collections %{collection_namespace}-%{collection_name}-%{collection_sap_install_version}.tar.gz

%if 0%{?fedora} || 0%{?rhel} >= 8
%{!?ansible_collection_files:%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}/}
%else
%if %{?ansible_collection_files:0}%{!?ansible_collection_files:1}
%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}/
%endif
%endif

%description
Collection of Ansible roles which configures a RHEL system according
to applicable SAP notes so that any SAP software can be installed.

%if %{with collection_artifact}
%package collection-artifact
Summary: Collection artifact to import to Automation Hub / Ansible Galaxy

%description collection-artifact
Collection artifact for %{name}. This package contains:
 * %{collection_namespace}-%{collection_name}-%{collection_sap_install_version}.tar.gz
 * %{collection_namespace}-%{collection_name_sap_infrastructure}-%{collection_sap_infrastructure_version}.tar.gz
%endif

%prep
%setup -q -a1 -a2 -n auto-maintenance-%{mainid}

pushd %{github_repo_sap_install}-%{commit_id_sap_install}/
%patch -P1 -p1
%patch -P2 -p1
%patch -P3 -p1
popd

# unpack collection tar file
%if %{without collection}
mkdir -p %{collection_dir_sap_install} %{collection_dir_sap_infrastructure}
# sap_install collection
tar zxf %{github_repo_sap_install}-%{commit_id_sap_install}/%{collection_namespace}-%{collection_name_sap_install}-%{collection_sap_install_version}.tar.gz -C %{collection_dir_sap_install}
# sap_install collection, Fix check mode
pushd %{collection_dir_sap_install}/
%patch -P3 -p1
popd
# sap_infrastructure collection
tar zxf %{github_repo_sap_infrastructure}-%{commit_id_sap_infrastructure}/%{collection_namespace}-%{collection_name_sap_infrastructure}-%{commit_id_sap_infrastructure}.tar.gz -C %{collection_dir_sap_infrastructure}
%endif

# remove zero file and symlinks
find . -type f -size 0 -delete
find . -type l -delete

# fix python and bash shebangs
find -type f \( -iname "*.py" \) -exec sed -i '1s=^#! */usr/bin/\(python\|env python\)[23]\?=#!/usr/bin/python3=' {} +
find -type f \( -iname "*.sh" \) -exec sed -i '1s=^#! */bin/bash=#!/usr/bin/bash=' {} +

# remove json files, these are already present in the collection subdirectory
rm -f FILES.json MANIFEST.json

%build
%if %{with collection}
# create dest-path
mkdir .collections

for role in %{rolename_sap_install} ; do
LANG=en_US.utf-8 LC_ALL=en_US.utf-8 python3 lsr_role2collection.py --role "$role" \
    --src-path %{github_repo_sap_install}-%{commit_id_sap_install}/roles/"$role" \
    --src-owner %{src_owner} \
    --dest-path .collections \
    --namespace %{collection_namespace} \
    --collection %{collection_name} \
    --new-role "$new_role"
done

# update galaxy.yml
cp -p galaxy.yml .collections/ansible_collections/%{collection_namespace}/%{collection_name}

# update README.md 
cp %{github_repo_sap_install}-%{commit_id_sap_install}/README.md .collections/ansible_collections/%{collection_namespace}/%{collection_name}

# Build the collection
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
%ansible_collection_build
popd
%endif

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ansible/roles \
         $RPM_BUILD_ROOT%{_pkgdocdir}

# create file selection for documents
echo "%dir %{_pkgdocdir}" > files_section.txt
cp %{github_repo_sap_install}-%{commit_id_sap_install}/README.md $RPM_BUILD_ROOT%{_pkgdocdir}/

echo "%{_pkgdocdir}/README.*" >> files_section.txt

# install sap_install roles
for role in %{rolename_sap_install} ; do
  mkdir -p $RPM_BUILD_ROOT%{_pkgdocdir}/$role
  cp -pR %{github_repo_sap_install}-%{commit_id_sap_install}/roles/$role $RPM_BUILD_ROOT%{_datadir}/ansible/roles/
  install -m 644 %{github_repo_sap_install}-%{commit_id_sap_install}/LICENSE \
    %{github_repo_sap_install}-%{commit_id_sap_install}/roles/$role/README.md \
    $RPM_BUILD_ROOT%{_pkgdocdir}/$role/
  echo "%dir %{_pkgdocdir}/$role" >> files_section.txt
done

# install sap_infrastructure roles
for role in %{rolename_sap_infrastructure} ; do
  mkdir -p $RPM_BUILD_ROOT%{_pkgdocdir}/$role
  cp -pR %{github_repo_sap_infrastructure}-%{commit_id_sap_infrastructure}/roles/$role $RPM_BUILD_ROOT%{_datadir}/ansible/roles/
  install -m 644 %{github_repo_sap_infrastructure}-%{commit_id_sap_infrastructure}/LICENSE \
    %{github_repo_sap_infrastructure}-%{commit_id_sap_infrastructure}/roles/$role/README.md \
    $RPM_BUILD_ROOT%{_pkgdocdir}/$role/
  echo "%dir %{_pkgdocdir}/$role" >> files_section.txt
done

# install collection files
%if %{with collection}
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
%ansible_collection_install
popd
%else
cp -pR collections $RPM_BUILD_ROOT%{_datadir}/ansible/
%endif

# install collection_artifact
%if %{with collection_artifact}
# Copy collection artifact to /usr/share/ansible/collections/ for collection-artifact
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name_sap_install}/
mv %{collection_namespace}-%{collection_name_sap_install}-%{collection_sap_install_version}.tar.gz \
$RPM_BUILD_ROOT%{_datadir}/ansible/collections/
popd
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name_sap_infrastructure}/
mv %{collection_namespace}-%{collection_name_sap_infrastructure}-%{collection_sap_infrastructure_version}.tar.gz \
$RPM_BUILD_ROOT%{_datadir}/ansible/collections/
popd
%endif

%files -f files_section.txt
%dir %{_datadir}/ansible
%dir %{_datadir}/ansible/roles
%doc %{_pkgdocdir}/*/README.md
%license %{_pkgdocdir}/*/LICENSE
%{_datadir}/ansible/roles/*
%if %{with collection}
%{ansible_collection_files}
%else
%{_datadir}/ansible/collections
%endif
%if %{with collection_artifact}
%files collection-artifact
%{_datadir}/ansible/collections/%{collection_namespace}-%{collection_name_sap_install}-%{collection_sap_install_version}.tar.gz
%{_datadir}/ansible/collections/%{collection_namespace}-%{collection_name_sap_infrastructure}-%{collection_sap_infrastructure_version}.tar.gz
%endif

%changelog
* Wed Feb 05 2025 Than Ngo <than@redhat.com> - 3.8.5-1
- Fix check mode for largesend.conf on ppc64le
- Fix the package name of the RHEL 10 Power tools
  Related: RHEL-73378

* Tue Feb 04 2025 Than Ngo <than@redhat.com> - 3.8.4-1
- Related: RHEL-73378, Fix checkmode for sysctl and in sap_hana_preconfigure

* Wed Jan 29 2025 Than Ngo <than@redhat.com> - 3.8.2-1
- Resolves: RHEL-73378, new collcection redhat.sap_install, redhat.sap_infrastructure

* Tue May 14 2024 Than Ngo <than@redhat.com> - 3.7.0-1
- Resolves: RHEL-35274, rebase sap roles to version 1.3.7

* Thu May 02 2024 Than Ngo <than@redhat.com> - 3.6.0-5
- Resolves: RHEL-33364, import and build for rhel-10-beta
